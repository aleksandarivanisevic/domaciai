(function ($){
    $.fn.slider = function(options){
      
        var slikeDefault = [
            {src:"slike/1.jpg",naslov:"naslov1",opis:"opis1"},{src:"slike/2.jpg",naslov:"naslov2",opis:"opis2"},
            {src:"slike/3.jpg",naslov:"naslov3",opis:"opis3"},
            {src:"slike/4.jpg",naslov:"naslov4",opis:"opis4"},
            {src:"slike/5.jpg",naslov:"naslov5",opis:"opis5"},{src:"slike/6.jpg",naslov:"naslov6",opis:"opis6"},{src:"slike/7.jpg",naslov:"naslov7",opis:"opis7"},{src:"slike/8.jpg",naslov:"naslov8",opis:"opis8"},{src:"slike/9.jpg",naslov:"naslov9",opis:"opis9"}
        ];
        var stringSlikeDefault = JSON.stringify(slikeDefault);
        
        var podesavanja = $.extend({
            color: "#00FF00",
            brojSlika: 9,
            debljinaOkvira: '2px',
            niz: stringSlikeDefault,
            autoplayMiliSec: 3000
        }, options);
        
        
        
        
        //var js = JSON.stringify(slike);
        slike = JSON.parse(podesavanja.niz);
        var slikeDuzina = slike.length;
        var trenutnaSlika = 1;
        var brojacPreskocenih = 0;
        
        for(var i=0; i<podesavanja.brojSlika;i++){
            if(i%(slikeDuzina+1) !== slikeDuzina){
                $('#male_slike').append('<img src="'+slike[i%(slikeDuzina+1)].src+'" width=100 height=100 class="thumbs"/>');
            }
            else{
                brojacPreskocenih++;
            }
        }
        for(var j=0; j<brojacPreskocenih; j++){
            $('#male_slike').append('<img src="'+slike[i%(slikeDuzina+1)].src+'" width=100 height=100 class="thumbs"/>');
        }
        
        var source = $('.thumbs').eq(0).attr('src');
        $('#velika').attr('src', source);
        for(var i=0; i<slikeDuzina; i++){
            if(source === slike[i].src){
                $('h3').html(slike[i].naslov+'<br>');
                $('h5').html(slike[i].opis);
            }
        }
        
        $('.thumbs').eq(0).css({
               borderColor : podesavanja.color,
               borderStyle : 'solid',
               borderWidth : podesavanja.debljinaOkvira
           });
        
        $('.thumbs').click(function(){
          
            clearInterval(autoplay);
            
            $('.thumbs').css({
               border : '' 
            });
    
            var source = $(this).attr('src');
            for(var i=0; i<slikeDuzina; i++){
                if(source === slike[i].src){
                    $('h3').html(slike[i].naslov+'<br>');
                    $('h5').html(slike[i].opis);
                }
            }
            
            $('#velika').attr("src", source);
            
            
            
            $(this).css({
               borderColor : podesavanja.color,
               borderStyle : 'solid',
               borderWidth : podesavanja.debljinaOkvira
           });
           
            trenutnaSlika = $(this).index()+1;
            $('p').html(trenutnaSlika);
          
            autoplay = setInterval(function(){
                $('#desno').trigger('click');    
            },podesavanja.autoplayMiliSec);
            
        });
        
        $('#desno').click(function(){
            
            var broj_slicica = $('.thumbs').length;
            if(trenutnaSlika === broj_slicica){
                $('.thumbs').eq(0).trigger('click');
            }
            else{
                $('.thumbs').eq(trenutnaSlika).trigger('click');
            }
            
        });
        
        $('#levo').click(function(){
            $('.thumbs').eq(trenutnaSlika-2).trigger('click');
            //eq(-1) je poslednji element tako da nema potrebe da se dodaje  uslov ako se klikne levo kada je na prvoj slici
        });
        
        $('#velika').css({
            borderColor : podesavanja.color,
            borderStyle : 'solid',
            borderWidth : podesavanja.debljinaOkvira
        });
        
        var autoplay = setInterval(function(){
            $('#desno').trigger('click');    
        },podesavanja.autoplayMiliSec);
        
        return this;
        
    };
    
}(jQuery));