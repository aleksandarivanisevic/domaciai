$(function(){
 
    var slike = [
            {src:"slike/pas.jpg",naslov:"pas",opis:"opis psa"},{src:"slike/tigar.jpg",naslov:"tigar",opis:"opis tigra"},
            {src:"slike/pingvin.jpg",naslov:"pingvin",opis:"opis pingvina"},
            {src:"slike/nosorog.jpg",naslov:"nosorog",opis:"opis nosoroga"},
            {src:"slike/zirafa.jpg",naslov:"zirafa",opis:"opis zirafe"}
        ];
    
    var strSlike = JSON.stringify(slike);
    
    $().slider({
        color : '#6eB1EF',
        brojSlika : 5,
        debljinaOkvira : '9px',
        niz: strSlike,
        autoplayMiliSec: 3000
    });
});