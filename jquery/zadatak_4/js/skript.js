$(function(){
    var trenutniMesec, trenutnaGodina, prviUmesecu;
    
    $("#datum").html("0");

    setInterval(function(){
        $('#format').trigger('change');
    }, 1000);

    $('#format').change(function(){
            var ispisDatuma;
            var format = this;
            var datum = new Date();
            var dan = datum.getDate(), mesec = datum.getMonth()+1, godina = datum.getFullYear(), sat = datum.getHours(), minut = datum.getMinutes(), sekund = datum.getSeconds();

            if(sat<10){
                sat = "0"+sat;
            }
            if(minut<10){
                minut = "0"+minut;
            }
            if(sekund<10){
                sekund = "0"+sekund;
            }
            if(dan<10){
                dan = "0"+dan;
            }
            if(mesec<10){
                mesec = "0"+mesec;
            }

            $("#vreme").html(sat+":"+minut+":"+sekund);

            if("nas" === format.value.toString()){
                ispisDatuma = datum.getDate()+"."+(datum.getMonth()+1)+"."+datum.getFullYear();
            }
            else if("evropa" === format.value.toString()){
                ispisDatuma = datum.getFullYear()+"-"+(datum.getMonth()+1)+"-"+datum.getDate();
            }
            else if("ameri" === format.value.toString()){
                ispisDatuma = (datum.getMonth()+1)+"/"+datum.getDate()+"/"+datum.getFullYear();
            }

            $("#datum").html(ispisDatuma);
    });
    $('#format').trigger('change');

    var datum = new Date();
    var meseci = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
    var dani = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    trenutniMesec = datum.getMonth();
    trenutnaGodina = datum.getFullYear();
    var brojDana = dani[trenutniMesec];

    if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
        brojDana=29;//ako je mesec februar i godina je prestupna
    }

    $("#sredina").html(meseci[trenutniMesec]+", "+trenutnaGodina);
    
    var trenutno = meseci[trenutniMesec]+", "+trenutnaGodina;
    
    $('#levo').click(function(){
        if(trenutniMesec>0){
            trenutniMesec--;
        }
        else{
            trenutniMesec = 11;
            trenutnaGodina--;
        }
        brojDana = dani[trenutniMesec];

    if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
        brojDana=29;//ako je mesec februar i godina je prestupna
    }   
        $("#sredina").html(meseci[trenutniMesec]+", "+trenutnaGodina);
        puniTabelu();
    }
    );

    $('#desno').click(function(){
        if(trenutniMesec<11){
            trenutniMesec++;
        }
        else{
            trenutniMesec = 0;
            trenutnaGodina++;
        }

        brojDana = dani[trenutniMesec];

        if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
            brojDana=29;//ako je mesec februar i godina je prestupna
        }
    
        $("#sredina").html(meseci[trenutniMesec]+", "+trenutnaGodina);
        puniTabelu();
    }
    );
    
function puniTabelu(){
    brisi();

    prviUmesecu = new Date(trenutnaGodina, trenutniMesec, 1).getDay();//pon = 0, ned = 6

    if(prviUmesecu>0)
        prviUmesecu--;
    else if(prviUmesecu===0)
        prviUmesecu=6;

    var i =1;
    var prvaNedelja = $(".prvaNedelja");

    for(var j = prviUmesecu; j<7; j++){
        $(prvaNedelja[j]).html(i++);
        $(prvaNedelja[j]).removeClass('siva');
        $(prvaNedelja[j]).addClass('bela');
    }

    var nedelja234 = $(".Nedelja");

    for(var j=0; j<21; j++){
        $(nedelja234[j]).html(i++);
        $(nedelja234[j]).removeClass('siva');
        $(nedelja234[j]).addClass('bela');
    }

    var petaNedelja = $(".petaNedelja");

    for(var j=0; j<7; j++){
        if(i>brojDana)
            break;
        $(petaNedelja[j]).html(i++);
        $(petaNedelja[j]).removeClass('siva');
        $(petaNedelja[j]).addClass('bela');
    }

    var sestaNedelja = $(".sestaNedelja");

    for(var j=0; j<7, i<=brojDana; j++){
        $(sestaNedelja[j]).html(i++);
        $(sestaNedelja[j]).removeClass('siva');
        $(sestaNedelja[j]).addClass('bela');
    }

    var niz = $('.brisi');
       console.log(prviUmesecu);
    if(sredina === trenutno){
        x = prviUmesecu+datum.getDate()-1;
        $(niz[x]).addClass('crvena');
    }
    else{
        $(niz[x]).removeClass('crvena');
    }
}

puniTabelu();

    function brisi(){
        sredina = $('#sredina').html();
        var niz = $('.brisi');

        for(var i=0; i<niz.length; i++){
            $(niz[i]).html('');
            $(niz[i]).removeClass('bela');
            $(niz[i]).addClass('siva');
        }

    }

});