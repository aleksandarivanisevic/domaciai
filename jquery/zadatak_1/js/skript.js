$(function(){ 
    
    localStorage.removeItem('prviBroj');
    localStorage.removeItem('znak');
    localStorage.removeItem('drugiBroj');
    localStorage.removeItem('unarni');
    
    $('.broj').click(function(){
        $('#text').removeClass('zacrni');
        //kad se klikne broj
        if(!localStorage.znak){
            //ako u localStorage-u nije znak, to znaci da se prvi broj gradi
            if(!localStorage.prviBroj){
                localStorage.prviBroj = 0;
            }
            localStorage.prviBroj = localStorage.prviBroj*10 + parseFloat(this.value);
            $('#text').html(localStorage.prviBroj);
            //localStorage.prviBroj = prviBroj;
        }
        else if(localStorage.znak){
            //ako u localStorage-u ima znak, gradi se drugi broj
            if(!localStorage.drugiBroj){
                localStorage.drugiBroj = 0;
            }
            
            localStorage.drugiBroj = localStorage.drugiBroj*10 + parseFloat(this.value);
            $('#text').html(localStorage.prviBroj + localStorage.znak + localStorage.drugiBroj);
        //    localStorage.drugiBroj = drugiBroj;
        }
        
    });
    
    $('.znak').click(function(){
        var znak = this.value, medjurezultat;
        $('#text').removeClass('zacrni');
        if(!localStorage.prviBroj){
                localStorage.prviBroj = 0;
                localStorage.znak = znak;
        }
        if(localStorage.drugiBroj){
            //ako postoji drugi broj, u localStorage.prviBroj se smesta medjurezultat
            switch(localStorage.znak){
                case '+': medjurezultat = parseFloat(localStorage.prviBroj) + parseFloat(localStorage.drugiBroj);
                    break;
                case '-': medjurezultat = parseFloat(localStorage.prviBroj) - parseFloat(localStorage.drugiBroj);
                    break;
                case '*': medjurezultat = parseFloat(localStorage.prviBroj) * parseFloat(localStorage.drugiBroj);
                    break;
                case '/':
                    if(localStorage.drugiBroj === '0'){
                        var deljenjeNulom = 1;
                    }
                    else{
                        medjurezultat = parseFloat(localStorage.prviBroj) / parseFloat(localStorage.drugiBroj);
                    }
                    break;
            }
            if(deljenjeNulom){
                window.alert('Nije moguce deliti nulom');
                localStorage.removeItem('prviBroj');
                localStorage.removeItem('drugiBroj');
                localStorage.removeItem('znak');
                localStorage.removeItem('unarni');
                $('#text').html('');
            }
            else{
                localStorage.prviBroj = medjurezultat;
                localStorage.removeItem('drugiBroj');
                drugiBroj = 0;
                $('#text').addClass('zacrni');
            }
        }
        if(deljenjeNulom){
            
        }
        else{
            localStorage.znak = znak;
            $('#text').html(localStorage.prviBroj + localStorage.znak); 
        }
    });
    
    $('#jednako').click(function(){
        if(localStorage.drugiBroj){
            var medjurezultat;
            switch(localStorage.znak){
                case '+': medjurezultat = parseFloat(localStorage.prviBroj) + parseFloat(localStorage.drugiBroj);
                    break;
                case '-': medjurezultat = parseFloat(localStorage.prviBroj) - parseFloat(localStorage.drugiBroj);
                    break;
                case '*': medjurezultat = parseFloat(localStorage.prviBroj) * parseFloat(localStorage.drugiBroj);
                    break;
                case '/': 
                    if(localStorage.drugiBroj === '0'){
                        var deljenjeNulom = 1;
                    }
                    else{
                        medjurezultat = parseFloat(localStorage.prviBroj)/parseFloat(localStorage.drugiBroj);
                    }
                    break;
            }
            if(deljenjeNulom){
                window.alert('Nije moguce deliti nulom');
                localStorage.removeItem('prviBroj');
                localStorage.removeItem('drugiBroj');
                localStorage.removeItem('znak');
                localStorage.removeItem('unarni');
                $('#text').html('');
            }
            else{
                localStorage.prviBroj = medjurezultat;
                localStorage.removeItem('drugiBroj');
                localStorage.removeItem('znak');
                drugiBroj = 0;
                $('#text').addClass('zacrni');
                $('#text').html(localStorage.prviBroj);
            }
        }
        else if(localStorage.prviBroj){
            if(localStorage.unarni){
                if(localStorage.unarni === 'K'){
                    if(localStorage.prviBroj < 0){
                        window.alert('Ne postoji koren iz negativnog broja');
                        localStorage.removeItem('prviBroj');
                        localStorage.removeItem('drugiBroj');
                        localStorage.removeItem('znak');
                        localStorage.removeItem('unarni');
                        $('#text').html('');
                    }
                    else{
                        localStorage.prviBroj = Math.sqrt(localStorage.prviBroj);
                    }
                }
                else if(localStorage.unarni === '^'){
                    localStorage.prviBroj = Math.pow(localStorage.prviBroj, 2);
                }
                localStorage.removeItem('unarni');
            }
            $('#text').addClass('zacrni');
            $('#text').html(localStorage.prviBroj);        
        }
    });
    
    $('.unarni').click(function(){
        if(localStorage.drugiBroj){
            var medjurezultat;
            switch(localStorage.znak){
                case '+': medjurezultat = parseFloat(localStorage.prviBroj) + parseFloat(localStorage.drugiBroj);
                    break;
                case '-': medjurezultat = parseFloat(localStorage.prviBroj) - parseFloat(localStorage.drugiBroj);
                    break;
                case '*': medjurezultat = parseFloat(localStorage.prviBroj) * parseFloat(localStorage.drugiBroj);
                    break;
                case '/': medjurezultat = parseFloat(localStorage.prviBroj) / parseFloat(localStorage.drugiBroj);
                    break;
            }
            localStorage.prviBroj = medjurezultat;
            if(this.value === 'K'){
                $('#text').html('sqrt(' + localStorage.prviBroj + ')');
                localStorage.unarni = 'K';
            }
            else if(this.value === '^'){
                $('#text').html(localStorage.prviBroj+ '^2');
                localStorage.unarni = '^';
            }
            localStorage.removeItem('znak');
            localStorage.removeItem('drugiBroj');
        }
        
        else if(localStorage.znak){
            localStorage.removeItem('znak');
            if(this.value === 'K'){
                $('#text').html('sqrt(' + localStorage.prviBroj + ')');
                localStorage.unarni = 'K';
            }
            else if(this.value === '^'){
                $('#text').html(localStorage.prviBroj+ '^2');
                localStorage.unarni = '^';
            }
        }
        else if(localStorage.unarni){
            if(this.value === localStorage.unarni){
                //unet je kvadrat nakon kvadrata simetricno za koren
                if(this.value === 'K'){
                    if(localStorage.prviBroj<0){
                        window.alert('Ne postoji koren iz negativnog broja');
                        localStorage.removeItem('prviBroj');
                        localStorage.removeItem('drugiBroj');
                        localStorage.removeItem('znak');
                        localStorage.removeItem('unarni');
                        $('#text').html('');
                    }
                    else{
                        localStorage.prviBroj = Math.sqrt(parseFloat(localStorage.prviBroj));
                         $('#text').html('sqrt(' + localStorage.prviBroj + ')');
                    }
                }
                else if(this.value === '^'){
                    localStorage.prviBroj = Math.pow(parseFloat(localStorage.prviBroj), 2);
                    $('#text').html(localStorage.prviBroj+ '^2');
                }
            }
            else if(this.value !== localStorage.unarni){
                //unet je koren nakon kvadrata ili obratno
                localStorage.unarni = this.value;
                if(localStorage.unarni === 'K'){
                    $('#text').html('sqrt(' + localStorage.prviBroj + ')');
                }
                else if(localStorage.unarni === '^'){
                    $('#text').html(localStorage.prviBroj + '^2');
                }
            }
        }
        else if(localStorage.prviBroj){
            if(this.value === 'K'){
                $('#text').html('sqrt(' + localStorage.prviBroj + ')');
                localStorage.unarni = 'K';
            }
            else if(this.value === '^'){
                $('#text').html(localStorage.prviBroj+ '^2');
                localStorage.unarni = '^';
            }
        }
        
    });
    
    $('#CE').click(function(){
       localStorage.removeItem('prviBroj'); 
       localStorage.removeItem('drugiBroj'); 
       localStorage.removeItem('znak'); 
       localStorage.removeItem('unarni');
        $('#text').html('');
    });
    
    $('#C').click(function(){
        if(localStorage.drugiBroj>9){
            localStorage.drugiBroj = Math.floor(localStorage.drugiBroj/10);
            $('#text').html(localStorage.prviBroj + localStorage.znak + localStorage.drugiBroj);
        }
        else if(localStorage.drugiBroj<10){
            localStorage.removeItem('drugiBroj');
            $('#text').html(localStorage.prviBroj + localStorage.znak);
        }
        else if(localStorage.znak || localStorage.unarni){
            localStorage.removeItem('znak');
            localStorage.removeItem('unarni');
            $('#text').html(localStorage.prviBroj);
        }
        else if(localStorage.prviBroj>9){
            localStorage.prviBroj = Math.floor(localStorage.prviBroj/10);
            $('#text').html(localStorage.prviBroj);
        }
        else{
            localStorage.removeItem('prviBroj');
            $('#text').html('');
        }
    });
    
});