var regexLink = /http[s]?[:][/]{2}[a-zA-Z0-9]+[.][^ \n\t]+/g;
var regexManki = /@[a-zA-Z0-9]+/g;
var regexHash = /#[a-zA-Z0-9]+/g;
var link, manki, hashtag;

$(function(){

    $('#parsiraj').click(function(){
        var tekst = $('#ulaz').val();
        var linkovi= [], mankiji = [], hashtagovi = [];

        while(link = regexLink.exec(tekst)){
            linkovi.push(link);
        }

        while(manki=regexManki.exec(tekst)){
            mankiji.push(manki);
        }

        while(hashtag=regexHash.exec(tekst)){
            hashtagovi.push(hashtag);
        }

        for(var i=0; i<linkovi.length; i++){
            tekst = tekst.replace(linkovi[i], '<a href="' + linkovi[i] + '">' + linkovi[i] + '</a>');
        }

        for(var i=0; i<mankiji.length; i++){
            tekst = tekst.replace(mankiji[i], '<a href="http://twitter.com/' + mankiji[i].toString().slice(1) + '/" class="manki"> ' + mankiji[i] + '</a>');
        }

        tekst = tekst.replace(/@/g, '<img src="slike/twitter.png" height="15" width="15"/>').replace(/[\n]/g, '<br>');

        for(var i=0; i<hashtagovi.length; i++){
            tekst = tekst.replace(hashtagovi[i], '<a href="http://twitter.com/' + hashtagovi[i].toString() + '/" class="hashtag"> ' + hashtagovi[i] + '</a>');
        }

        $('#izlaz').html(tekst);

    });

});