var kontakti = [];

var text = '{"kontaktiDefault":[' +
'{"ime":"Pera","prezime":"Peric","ulica":"Bulevar Milutina Milankovica", "broj":"37a","telefon":"011123456","mobilni":"060123456" },' +
'{"ime":"Laza","prezime":"Lazic","ulica":"Ustanicka","broj":"bb","telefon":"011/765-43-21","mobilni":"063/765-43-21" }]}';

obj = JSON.parse(text);
kontakti[0] = {ime:obj.kontaktiDefault[0].ime, prezime:obj.kontaktiDefault[0].prezime, ulica:obj.kontaktiDefault[0].ulica, broj:obj.kontaktiDefault[0].broj, telefon:obj.kontaktiDefault[0].telefon, mobilni:obj.kontaktiDefault[0].mobilni};

kontakti[1] = {ime:obj.kontaktiDefault[1].ime, prezime:obj.kontaktiDefault[1].prezime, ulica:obj.kontaktiDefault[1].ulica, broj:obj.kontaktiDefault[1].broj, telefon:obj.kontaktiDefault[1].telefon, mobilni:obj.kontaktiDefault[1].mobilni};

var tabela = document.getElementById("tabela");

var tr = document.createElement('tr');
var td = [];
var th = [];

td[0] = document.createElement('td');
td[0].innerHTML=kontakti[0].ime;

td[1] = document.createElement('td');
td[1].innerHTML=kontakti[0].prezime;

td[2] = document.createElement('td');
if(kontakti[0].ulica!=="" && kontakti[0].broj!=="")
    td[2].innerHTML=kontakti[0].ulica+", "+kontakti[0].broj;
else
    td[2].innerHTML=kontakti[0].ulica;
td[3] = document.createElement('td');
td[3].innerHTML=kontakti[0].telefon;
            
td[4] = document.createElement('td');
td[4].innerHTML=kontakti[0].mobilni;
            
for(var k=0; k<5; k++)
    tr.appendChild(td[k]);
tabela.appendChild(tr);   
        
tr=document.createElement('tr');

td[0] = document.createElement('td');
td[0].innerHTML=kontakti[1].ime;

td[1] = document.createElement('td');
td[1].innerHTML=kontakti[1].prezime;

td[2] = document.createElement('td');
if(kontakti[1].ulica!=="" && kontakti[1].broj!=="")
    td[2].innerHTML=kontakti[1].ulica+", "+kontakti[1].broj;
else
    td[2].innerHTML=kontakti[1].ulica;
td[3] = document.createElement('td');
td[3].innerHTML=kontakti[1].telefon;

td[4] = document.createElement('td');
td[4].innerHTML=kontakti[1].mobilni;

for(var k=0; k<5; k++)
    tr.appendChild(td[k]);
tabela.appendChild(tr);   
        
tr=document.createElement('tr');

var j=2;
var tabela = document.getElementById("tabela");
 
var ime, prezime, ulica, broj, telefon, mobilni;
function unos(){
    ime = document.getElementById("ime").value;
    prezime = document.getElementById("prezime").value;
    ulica = document.getElementById("ulica").value;
    broj = document.getElementById("broj").value;
    telefon = document.getElementById("telefon").value;
    mobilni = document.getElementById("mobilni").value;
    regexIme = /(^[A-Z][a-z]+$)/g;
    regexPrezime = /(^[A-Z][a-z]+$)/g;
    regexUlica = /[A-Z][a-z]+([ ][A-Z]?[a-z]+)*/g;
    regexBroj = /([0-9]+[A-Za-z]?)|[Bb][Bb]/g;
    regexTelefon = /(([+]381[ ]?[0-9][0-9])|(0[0-9][0-9]))[/ ]?[0-9]{3}([. -])?[0-9]{2}((\4[0-9]{2})|[0-9])/g;
    regexMobilni = /(([+][0-9]{3}[ ]?[6][0-9])|(06[0-9]))[/ ]?[0-9]{3}([. -])?[0-9]{2}((\4[0-9]{2})|[0-9])/g;
    
    if(regexIme.exec(ime)===null){
        if(ime!=="")
            window.alert("Los format imena!");
        else
            window.alert("Polja sa * moraju biti popunjena!");
    }
    else if(regexPrezime.exec(prezime)===null && prezime!==""){
        window.alert("Los format prezimena!");
    }
    else if(regexUlica.exec(ulica)===null && ulica!==""){
        window.alert("Los format ulice!");
    }
    else if(regexBroj.exec(broj)===null && broj!==""){
        window.alert("Los format broja!");
    }
    else if(regexTelefon.exec(telefon)===null){
        if(telefon!=="")
            window.alert("Los format telefona!");
        else
            window.alert("Polja sa * moraju biti popunjena!");
    }
    else if(regexMobilni.exec(mobilni)===null){
        if(mobilni!=="")
            window.alert("Los format telefona!");
        else
            window.alert("Polja sa * moraju biti popunjena!");
    }
    else{
        kontakti[j] = {ime:ime, prezime:prezime, ulica:ulica, broj:broj, telefon:telefon, mobilni:mobilni};
    }
        
    
    td[0] = document.createElement('td');
    td[0].innerHTML=kontakti[j].ime;

    td[1] = document.createElement('td');
    td[1].innerHTML=kontakti[j].prezime;

    td[2] = document.createElement('td');
    if(kontakti[j].ulica!=="" && kontakti[j].broj!=="")
        td[2].innerHTML=kontakti[j].ulica+", "+kontakti[j].broj;
    else
        td[2].innerHTML=kontakti[j].ulica;
    td[3] = document.createElement('td');
    td[3].innerHTML=kontakti[j].telefon;
    
    td[4] = document.createElement('td');
    td[4].innerHTML=kontakti[j].mobilni;

    for(var k=0; k<5; k++)
        tr.appendChild(td[k]);
    tabela.appendChild(tr);   
        
    tr=document.createElement('tr');

    j++;
    var radio = document.getElementsByName("izbor");
    if(radio[0].checked)
        sortiraj();
}

function sortiraj(){
    
    kontakti.sort(function(a, b) { 
        if(a.ime.localeCompare(b.ime)!==0)
            return a.ime.localeCompare(b.ime);
        else
            return a.prezime.localeCompare(b.prezime);
    });
    
    var list = document.getElementById("tabela");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    
    var tr=document.createElement('tr');
    
    th[0] = document.createElement('th');
    th[0].innerHTML="Ime";
    
    th[1] = document.createElement('th');
    th[1].innerHTML="Prezime";
    
    th[2] = document.createElement('th');
    th[2].innerHTML="Adresa";
    
    th[3] = document.createElement('th');
    th[3].innerHTML="Fiksni telefon";
    
    th[4] = document.createElement('th');
    th[4].innerHTML="Mobilni telefon";
    
    for(var k=0; k<5; k++)
        tr.appendChild(th[k]);
    tabela.appendChild(tr);
    
    for(var t=0; t<kontakti.length; t++){
        
        tr=document.createElement('tr');
        
        td[0] = document.createElement('td');
        td[0].innerHTML=kontakti[t].ime;

        td[1] = document.createElement('td');
        td[1].innerHTML=kontakti[t].prezime;

        td[2] = document.createElement('td');
        if(kontakti[t].ulica!=="" && kontakti[t].broj!=="")
            td[2].innerHTML=kontakti[t].ulica+", "+kontakti[t].broj;
        else
            td[2].innerHTML=kontakti[t].ulica;
        td[3] = document.createElement('td');
        td[3].innerHTML=kontakti[t].telefon;

        td[4] = document.createElement('td');
        td[4].innerHTML=kontakti[t].mobilni;

        for(var k=0; k<5; k++)
            tr.appendChild(td[k]);
        tabela.appendChild(tr);   

        
    }
    
}


function trazi() {

    var search, trazeni, table, tr, td, i;
    
    search = document.getElementById("search");
    trazeni = search.value.toUpperCase();
    table = document.getElementById("tabela");
    tr = table.getElementsByTagName("tr");
    
    for(i=1; i<tr.length; i++){
    
        if(/[a-zA-Z]/g.exec(trazeni.charAt(0))!==null || trazeni.charAt(0)===""){
            td1 = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];

            if(td1){
                if(td1.innerHTML.toUpperCase().substring(0, trazeni.length)===trazeni){
                    tr[i].style.display = "";
                }
                else if(td2.innerHTML.toUpperCase().substring(0, trazeni.length)===trazeni){
                    tr[i].style.display = "";
                }
                else{
                    tr[i].style.display = "none";
                }
            }
        }
        else{
            td1 = tr[i].getElementsByTagName("td")[3];
            td2 = tr[i].getElementsByTagName("td")[4]; 

            if(td1){
                if(td1.innerHTML.toUpperCase().substring(0, trazeni.length)===trazeni){
                    tr[i].style.display = "";
                }
                else if(td2.innerHTML.toUpperCase().substring(0, trazeni.length)===trazeni){
                    tr[i].style.display="";
                }
                else{
                    tr[i].style.display = "none";
                }
            }
        }
        
    }
}