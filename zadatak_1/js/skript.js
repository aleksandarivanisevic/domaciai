var br = 0;
var znak;
var ind = 0;
var br2 = 0;
var rezultat ;
var ind2 = 0;
var ind3 = 0;
var puta_podeljeno=0;

function cifra(k) {
    /*f-ja cifra(k) vraca da li je karakter k cifra */
    if(k>=0 && k<=9)
        return true;
    return false;
}

function kvadrat(x) {
    /*f-ja kvadrat(x) vraca kvadrat broja x*/
    return x*x;
}

function broj(i) {
    "use strict";
     document.getElementById("text").style.fontWeight = "normal";
     document.getElementById("text").style.color = "gray";
    
    if(cifra(i) && ind2===1){
        /*ako je uneta cifra a ind2 je 1, to znaci da je to unos cifre nakon znaka jednakosti pa se treba sve vratiti na pocetno stanje */
        br = i;
        br2 = 0;
        rezultat = 0;
        document.getElementById("text").innerHTML = br;
        ind2 = 0;
        ind=0;
        puta_podeljeno = 0;
    }
    
    else if(cifra(i) && ind2!==1){
        /*ako je uneta cifra a ind2 je 0, to znaci da je to regularan unos cifre*/
        if(ind===0){
            /*ako je ind 0 onda to znaci da nije unet aritmeticki znak pa se i dalje "gradi" prvi broj */
            br = br * 10 + i; 
            document.getElementById("text").innerHTML = br;
            puta_podeljeno = 0;
        }
        else{
            /*inace, nakon aritmetickog znaka se gradi drugi broj */
            br2 = br2 * 10 + i;
            document.getElementById("text").innerHTML = br + znak + br2;
            puta_podeljeno = 0;
        }
    }
    else if(i==='c'){
        /*ako je pritisnuto dugme CE sve se vraca na pocetno stanje */
        br=0;
        br2=0;
        ind=0;
        rezultat=0;
        ind2=0;
        document.getElementById("text").innerHTML = "";
        puta_podeljeno = 0;
    }
    else if(i==='C'){
        /*ako je pritisnuto dugme C brise se poslednji karakter */
        if(ind===0){
            /*ako je ind 0 onda jos nije unet znak pa se brisu cifre prvog broja */
            br = Math.floor(br/10);
            document.getElementById("text").innerHTML = br;
            puta_podeljeno = 0;
        }
        else{
            /*inace, */
            if(br2!==0){
                /*ako drugi broj nije ceo obrisan, brisu se njegove cifre */
                br2 = Math.floor(br2/10);
                if(br2!==0)
                    document.getElementById("text").innerHTML = br + znak + br2;
                else
                    document.getElementById("text").innerHTML = br + znak;
                puta_podeljeno = 0;
            }
            else{
                /*ako je obrisan ceo drugi broj, brise se znak*/
                ind=0;
                document.getElementById("text").innerHTML = br;
                puta_podeljeno = 0;
            }
        }
    }
    else if(!cifra(i) && i!=='=' && ind3===0){
        /*ako je pritisnuto dugme koje nije ni znak jednakosti ni cifra */
        ind=1;
        znak=i;
        ind2=0;
        if(znak!=='^' && znak!=='K'){
                /*ako je pritisnuto dugme koje nije ni kvadrat ni koren, ispisuje se znak nakon broja */
                document.getElementById("text").innerHTML = br + znak;
        }
        else if(znak==='^')
            /*ako je pritisnut kvadrat, ispisuje se broj ^ 2*/
            document.getElementById("text").innerHTML = br + znak + 2;
        else if(znak==='K')
            /*ako je pritisnut koren, ispisuje se sqrt(broj)*/
            document.getElementById("text").innerHTML = "sqrt(" + br + ")";
        
        ind3=1;
        
        
        if(znak==='*' || znak==='/')
            puta_podeljeno = 1;
        else
            puta_podeljeno = 0;
        
    }
    
    else if(!cifra(i) && i!=='=' && ind3===1){
        /* ind3 oznacava da je vec jednom bio aritmeticki znak a da nije nakon njega kucan znak jednakosti, pa se ovde 
        racuna medjurezultat */
        if(puta_podeljeno===1){
            br2=1;
        }
        switch(znak){
                /*racunamo rezultat u zavisnosti od aritmeticke operacije */
            case '+':
                br = br + br2;
                break;
            case '-':
                br = br - br2;
                break;
            case '*':
                br = br * br2;
                break;
            case '/':
                if(br2!==0)
                    br = br / br2;
                else{
                    window.alert("Nije dozvoljeno deliti nulom");
                    
                    broj('c');
                }
                break;
            case '^':
                br = kvadrat(br);
                break;
            case 'K':
                if(br>=0)
                    br = Math.sqrt(br);
                else{
                    alert("Koren iz negativnog broja nije definisan");
                    
                    broj('c');
                }
                break;
            default:
                br = br;
            }
        if(znak==='*' || znak==='/')
            puta_podeljeno=1;
        else
            puta_podeljeno=0;
        ind=1;
        znak=i;
        ind2=0;
        br2=0;
        if(znak!=='^' && znak!=='K')
            /*ako je pritisnuto dugme koje nije ni kvadrat ni koren, ispisuje se znak nakon broja */
            document.getElementById("text").innerHTML = br + znak;
        else if(znak==='^')
            /*ako je pritisnut kvadrat, ispisuje se broj ^ 2*/
            document.getElementById("text").innerHTML = br + znak + 2;
        else if(znak==='K')
            /*ako je pritisnut koren, ispisuje se sqrt(broj)*/
            document.getElementById("text").innerHTML = "sqrt(" + br + ")";
        
        
    }
    
    else if(i==='=' && ind2===0){
        /*ako je pritisnut znak jednakosti, a ind2 je 0, sto znaci da se prvi put u nizu pritiska jednakost*/
        switch(znak){
                /*racunamo rezultat u zavisnosti od aritmeticke operacije */
            case '+':
                rezultat = br + br2;
                break;
            case '-':
                rezultat = br - br2;
                break;
            case '*':
                rezultat = br * br2;
                break;
            case '/':
                if(br2!==0)
                    rezultat = br / br2;
                else{
                    window.alert("Nije dozvoljeno deliti nulom");

                    broj('c');
                }
                break;
            case '^':
                rezultat = kvadrat(br);
                break;
            case 'K':
                if(br>=0)
                    rezultat = Math.sqrt(br);
                else{
                    alert("Koren iz negativnog broja nije definisan");

                    broj('c');
                }
                break;
            default:
                rezultat = br;
            }
        
        document.getElementById("text").style.fontWeight = "bolder";
        document.getElementById("text").style.color = "black";
        document.getElementById("text").innerHTML = rezultat;
       /*menja se stil ispisa i ispisuje rezultat */
        br = rezultat;
        br2 = 0;
        ind2 = 1;/*podesavamo indikator da se u slucaju ponovnog pritiskanja znaka jednakosti sve obrise */
        ind3 = 0;
        puta_podeljeno=0;
    }
    
    else if(i==='=' && ind2===1){
        /*ako je pritisnut znak jednakosti, ali ne prvi put u nizu, brise se sve */
        document.getElementById("text").innerHTML = "";
        br = 0;
        rezultat = 0;
        br2 = 0;
        ind = 0;
        puta_podeljeno=0;
    }
    
}
