var x = 0;
format();
document.getElementById("datum").innerHTML = x;

setInterval(function(){
     format();
}, 1000);

function format(){
        var format = document.getElementById("format");
        var datum = new Date();
        var dan = datum.getDate(), mesec = datum.getMonth()+1, godina = datum.getFullYear(), sat = datum.getHours(), minut = datum.getMinutes(), sekund = datum.getSeconds();
        
        if(sat<10){
            sat = "0"+sat;
        }
        if(minut<10){
            minut = "0"+minut;
        }
        if(sekund<10){
            sekund = "0"+sekund;
        }
        if(dan<10){
            dan = "0"+dan;
        }
        if(mesec<10){
            mesec = "0"+mesec;
        }
    
        document.getElementById("vreme").innerHTML = sat+":"+minut+":"+sekund;

        if("nas" === format.value.toString()){
            x = datum.getDate()+"."+(datum.getMonth()+1)+"."+datum.getFullYear();
        }
        else if("evropa" === format.value.toString()){
            x = datum.getFullYear()+"-"+(datum.getMonth()+1)+"-"+datum.getDate();
        }
        else if("ameri" === format.value.toString()){
            x = (datum.getMonth()+1)+"/"+datum.getDate()+"/"+datum.getFullYear();
        }
        document.getElementById("datum").innerHTML = x;
}


var datum = new Date();
var meseci = ["Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
var dani = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

var trenutniMesec = datum.getMonth();
var trenutnaGodina = datum.getFullYear();
var brojDana = dani[trenutniMesec];

if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
    brojDana=29;//ako je mesec februar i godina je prestupna
}

document.getElementById("sredina").innerHTML = meseci[trenutniMesec]+", "+trenutnaGodina;

function levo(){
    if(trenutniMesec>0){
        trenutniMesec--;
    }
    else{
        trenutniMesec = 11;
        trenutnaGodina--;
    }
    brojDana = dani[trenutniMesec];

if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
    brojDana=29;//ako je mesec februar i godina je prestupna
}   
    document.getElementById("sredina").innerHTML = meseci[trenutniMesec]+", "+trenutnaGodina;
    puniTabelu();
}

function desno(){
    if(trenutniMesec<11){
        trenutniMesec++;
    }
    else{
        trenutniMesec = 0;
        trenutnaGodina++;
    }
 
  brojDana = dani[trenutniMesec];

if(trenutniMesec===1 && ((trenutnaGodina%4===0 && trenutnaGodina%100!==0) || trenutnaGodina%400===0) ){
    brojDana=29;//ako je mesec februar i godina je prestupna
}
    
    document.getElementById("sredina").innerHTML = meseci[trenutniMesec]+", "+trenutnaGodina;
    puniTabelu();
}

function puniTabelu(){
    brisi();
    
    var prviUmesecu = new Date(trenutnaGodina, trenutniMesec, 1).getDay();//pon = 0, ned = 6
    if(prviUmesecu>0)
        prviUmesecu--;
    else if(prviUmesecu===0)
        prviUmesecu=6;
    var prvaNedelja = document.getElementsByClassName("prvaNedelja");
    var i =1;


    for(var j = prviUmesecu; j<7; j++){
        prvaNedelja[j].innerHTML = i++;
        prvaNedelja[j].style.backgroundColor = "#FFF";
    }

    var drugaNedelja = document.getElementsByClassName("drugaNedelja");

    for(var j=0; j<7; j++){
        drugaNedelja[j].innerHTML = i++;
        drugaNedelja[j].style.backgroundColor = "#FFF";
    }

    var trecaNedelja = document.getElementsByClassName("trecaNedelja");

    for(var j=0; j<7; j++){
        trecaNedelja[j].innerHTML = i++;
        trecaNedelja[j].style.backgroundColor = "#FFF";
    }

    var cetvrtaNedelja = document.getElementsByClassName("cetvrtaNedelja");

    for(var j=0; j<7; j++){
        cetvrtaNedelja[j].innerHTML = i++;
        cetvrtaNedelja[j].style.backgroundColor = "#FFF";
    }

    var petaNedelja = document.getElementsByClassName("petaNedelja");

    for(var j=0; j<7; j++){
        if(i>brojDana)
            break;
        petaNedelja[j].innerHTML = i++;
        petaNedelja[j].style.backgroundColor = "#FFF";
    }

    var sestaNedelja = document.getElementsByClassName("sestaNedelja");

    for(var j=0; j<7, i<=brojDana; j++){
        sestaNedelja[j].innerHTML = i++;
        sestaNedelja[j].style.backgroundColor = "#FFF";
    }
}

puniTabelu();

function brisi(){
    var niz = document.getElementsByClassName("brisi");
    for(var i=0; i<niz.length; i++){
        niz[i].innerHTML = "";
        niz[i].style.backgroundColor = "#f9f9f9";
    }
}