var ulaz = document.getElementById("ulaz");
var tekst;

var izlaz = document.getElementById("izlaz");

var regexLink = /http[s]?[:][/]{2}[a-zA-Z0-9]+[.][^ \n\t]+/g;
var regexManki = /@[a-zA-Z0-9]+/g;
var regexHash = /#[a-zA-Z0-9]+/g;
var link;
var manki;
var hashtag;


function stilizuj() {
    tekst=ulaz.value;
    var rezultat = tekst;
    var linkovi = [];
    var mankiji = [];
    var hashtagovi = [];
    
    while(link=regexLink.exec(tekst)){
        linkovi.push(link);
    }
  
    while(manki=regexManki.exec(tekst)){
        mankiji.push(manki);
    }
    
    while(hashtag=regexHash.exec(tekst)){
        hashtagovi.push(hashtag);
    }
    
    for(var i=0; i<linkovi.length; i++){
        rezultat = rezultat.replace(linkovi[i], '<a href="' + linkovi[i] + '">' + linkovi[i] + '</a>');
    }
    
    for(var i=0; i<mankiji.length; i++){
        rezultat = rezultat.replace(mankiji[i], '<a href="http://twitter.com/' + mankiji[i].toString().slice(1) + '/" class="manki"> ' + mankiji[i] + '</a>');
    }
    
    rezultat = rezultat.replace(/@/g, '<img src="slike/twitter.png" height="15" width="15"/>');
    
    rezultat = rezultat.replace(/[\n]/g, '<br>')
    
    for(var i=0; i<hashtagovi.length; i++){
        rezultat = rezultat.replace(hashtagovi[i], '<a href="http://twitter.com/' + hashtagovi[i].toString() + '/" class="hashtag"> ' + hashtagovi[i] + '</a>');
    }
    
    izlaz.innerHTML=rezultat;
    
}