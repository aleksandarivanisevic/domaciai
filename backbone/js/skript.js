var identifikatorPoruke = '';
var inputGreska = '';

var Osoba = Backbone.Model.extend({
    defaults: {
        ime: '',
        prezime: '',
        ulica: '',
        broj: '',
        telefon: '',
        mobilni: ''
    },
    validate: function(atributi){
        var regexIme = /(^[A-Z][a-z]+$)/g,
        regexPrezime = /(^[A-Z][a-z]+$)/g,
        regexUlica = /[A-Z][a-z]+([ ][A-Z]?[a-z]+)*/g,
        regexBroj = /([0-9]+[A-Za-z]?)|[Bb][Bb]/g,
        regexTelefon = /(([+]381[ ]?[0-9][0-9])|(0[0-9][0-9]))[/ ]?[0-9]{3}([. -]?)[0-9]{2}((\4[0-9]{2})|[0-9])/g,
        regexMobilni = /(([+][0-9]{3}[ ]?[6][0-9])|(06[0-9]))[/ ]?[0-9]{3}([. -]?)[0-9]{2}((\4[0-9]{2})|[0-9])/g;
        if(atributi.ime === '' || !regexIme.test(atributi.ime)){
            if(atributi.ime === ''){
                identifikatorPoruke = '#spanIme';
                inputGreska = '#ime';
                return "Polja sa * su obavezna";
            }
            else{
                identifikatorPoruke = '#spanIme';
                inputGreska = '#ime';
                return "Nije dobro ime";
            }
        }
        else if(!atributi.prezime === '' && !regexPrezime.test(atributi.prezime)){
            identifikatorPoruke = '#spanPrezime';
            inputGreska = '#prezime';
            return "Nije dobro prezime";
        }
        else if(!atributi.ulica === '' && !regexUlica.test(atributi.ulica)){
            identifikatorPoruke = '#spanUlica';
            inputGreska = '#ulica';
            return "Nije dobra ulica";
        }
        else if(!atributi.broj === '' && !regexBroj.test(atributi.broj)){
            identifikatorPoruke = '#spanBroj';
            inputGreska = '#broj';
            return "Nije dobar broj";
        }
        else if(atributi.telefon === '' || !regexTelefon.test(atributi.telefon)){
            if(atributi.telefon === ''){
                identifikatorPoruke = '#spanTelefon';
                inputGreska = '#telefon';
                return "Polja sa * su obavezna";
            }
            else{
                identifikatorPoruke = '#spanTelefon';
                inputGreska = '#telefon';
                return "Nije dobar telefon";
            }
        }
        else if(atributi.mobilni === '' || !regexMobilni.test(atributi.mobilni)){
            if(atributi.mobilni === ''){
                identifikatorPoruke = '#spanMobilni';
                inputGreska = '#mobilni';
                return "Polja sa * su obavezna";
            }    
            else{
                identifikatorPoruke = '#spanMobilni';
                inputGreska = '#mobilni';
                return "Nije dobar mobilni";
            }
        }
    }
});

var Imenik = Backbone.Collection.extend({
    model: Osoba
});

var OsobaView = Backbone.View.extend({
    tagName: 'tr',
    className: 'dinamickiRed',
    render: function(){
			    this.$el.append('<td>'+this.model.get("ime")+'</td><td>'+this.model.get("prezime")+'</td><td>'+this.model.get("ulica")+', '+this.model.get("broj")+'</td><td>'+this.model.get("telefon")+'</td><td>'+this.model.get("mobilni")+'</td>');
		    return this;
	}
});

var ImenikView = Backbone.View.extend({
    render: function(){
		var self = this;
        this.model.each(function(osoba){
		    var osobaView = new OsobaView({model:osoba});
           
        self.$el.append(osobaView.render().$el); 
		});
	}
});



$(function(){
    
    var imenik = new Imenik();
    
    $('#unos').click(function(){
        var osoba = new Osoba({
            ime : $('#ime').val(),
            prezime: $('#prezime').val(),
            ulica: $('#ulica').val(),
            broj: $('#broj').val(),
            telefon: $('#telefon').val(),
            mobilni: $('#mobilni').val()
        });
        if(!osoba.isValid()){
            $('span').html('');
            $('input').removeClass('error');
            $(identifikatorPoruke).html(osoba.validationError);
            $(inputGreska).addClass('error');
        }
        else{
            $('span').html('');
            $('input').removeClass('error');
            $('input').val('');
            imenik.add(osoba);
        }
        console.log(imenik);
        
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();

        
    });
    
    $('#thIme').click(function(){
        imenik.comparator = function(model){
            return model.get('ime');
        }
        imenik.sort();
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();
    });
    
    $('#thPrezime').click(function(){
        imenik.comparator = function(model){
            return model.get('prezime');
        }
        imenik.sort();
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();
    });
    
    $('#thAdresa').click(function(){
        imenik.comparator = function(model){
            return model.get('ulica')+'-'+model.get('broj');
        }
        imenik.sort();
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();
    });
    
    $('#thTelefon').click(function(){
        imenik.comparator = function(model){
            return model.get('telefon');
        }
        imenik.sort();
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();
    });
    
    $('#thMobilni').click(function(){
        imenik.comparator = function(model){
            return model.get('mobilni');
        }
        imenik.sort();
        $('.dinamickiRed').remove();
        
        var imenikView = new ImenikView({el:"#tabela",model:imenik});
        imenikView.render();
    });
});